College = function()
{
	
}

$('document').ready(function(){
	
	$('#save_btn').click(function(){
		$('#demo_modal').validate({
			rules:{
				first_name:{required :true},
				last_name:{required:true}
			},
			submitHandler:function(form){
				data = $('#demo_modal').serialize();
				$.ajax({
					url:base_url+'college/demo_method',
					type:'POST',
					data:data,
					beforeSend: function()
					{
						
						//$('.modal-body').css('opacity',0.9);
						//$('.modal-body').css('z-index',-1);
						$('#loading').show();
					},
					 complete: function(){
						$('#loading').hide();
					},
					"aoColumns": [
						{ "mDataProp": "name" },
						{ "mDataProp": "address" },
						{ "mDataProp": "phone"},
						{ "mDataProp": "pinCode"},
						{ "fnRender": function (o) {return '<a href=/Produto/Detalhar/' + o.aData[0] + '>' + 'More' + '</a>';}}
						],
					success: function(){
						
					}
				});
		}
	});
	
	});
	
	
	var oTable = $('#college_table').dataTable({
	            "bProcessing": true,
	            "bServerSide": true,
	            "sAjaxSource": base_url+'college/datatable',
	            "bJQueryUI": true,
	            "sPaginationType": "full_numbers",
	            "iDisplayStart ": 20,
	           	"fnInitComplete": function () {
	               
	            },
	            'fnServerData': function (sSource, aoData, fnCallback) {
	                $.ajax
	                ({
	                    'dataType': 'json',
	                    'type': 'POST',
	                    'url': sSource,
	                    'data': aoData,
	                    'success': fnCallback
	                });
	            }
	        });
	
	$('#collegeName').blur(function(){
		if ($('.validationForName').hasClass('has-error'))
		{
			$('.validationForName').removeClass('has-error');
		}
		
	});
	$('#address').blur(function(){
		if ($('.validationForAddress').hasClass('has-error'))
		{
			$('.validationForAddress').removeClass('has-error');
		}
		
	});
	$('#phone').blur(function(){
		if ($('.validationForPhone').hasClass('has-error'))
		{
			$('.validationForPhone').removeClass('has-error');
		}
		
	});
	$('#pinCode').blur(function(){
		if ($('.validationForCode').hasClass('has-error'))
		{
			$('.validationForCode').removeClass('has-error');
		}
		
	});
});
College.saveCollege = function()
{
	isValidate = College.validationForCollege();
	if (isValidate)
	{
		collegeData =$('#collegeForm').serialize();
		$.ajax({
			url:base_url+'college/saveCollegeDeails',
			type:'POST',
			data:collegeData,
			success:function(result)
			{
				if (result == 0)
				{
					window.redirect = base_url+'college';
				}
				else if (result != 0)
				{
					$('.alert-success').show();
					window.setTimeout(7000,function(){
						$('.alert-success').hide('slow');	
					})	
				}
			}
		})	
	}
	else
	{
		$('.alert-danger').show();
		window.setTimeout(function(){
			$('.alert-danger').hide('slow');	
		},7000);
	}
	
}
College.validationForCollege = function()
{
	var isValidate = true;
	if($('#collegeName').val()=="")
	{
		isValidate = false;
		$('.validationForName').addClass('has-error');
	}
	
	if($('#address').val()=="")
	{
		isValidate = false;
		$('.validationForAddress').addClass('has-error');
	}
	
	if($('#phone').val()=="")
	{
		isValidate = false;
		$('.validationForPhone').addClass('has-error');
	}
	
	if($('#pinCode').val()=="")
	{
		isValidate = false;
		$('.validationForCode').addClass('has-error');
	}
	return isValidate; 
}
