<?php
	class CollegeDetails extends CI_Model
	{
		function __construct()
		{
			parent::__construct();	
		}	
		 public function getDatatableRecord()
		 {
		 	  	$fields = array(
                        'preferences' => array('type' => 'TEXT')
				);
		 	  $this->datatables->select('name,address,phone,pinCode')
            						->unset_column('id')
            						->from('college');	 
        					echo $this->datatables->generate();	
	        
		 }
		 
		 public function getCollegeDeatilsWithId($id)
		 {
		 	$query = $this->db->get_where('college', array('id' => $id), null, null);
		 	return $query;
		 }	
		 public function addNewCollege($data)
		 {
		 	unset($data['collegeId']);
			$this->db->insert('college',$data);
		 }	
		 public function updateCollege($data)
		 {
		  	$collegeId = $data['collegeId'];
			unset($data['collegeId']);
			$this->db->where('id',$collegeId);
			$this->db->update('college',$data);	 
		 }
		
	}
?>