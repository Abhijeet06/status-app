<html>
    <head>
        <title>Login Details</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome/css/font-awesome.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/sb-admin.css'); ?>"/>

        <script>var base_url =  '<?php echo base_url(); ?>
			';
        </script>
        <script src="<?php echo base_url('assets/js/bootstrap.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/dataTables.bootstrap.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/sb-admin.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/jquery.metisMenu.js'); ?>" type="text/javascript"></script>
    </head>
    <body>
        <!--<h1>Simple Login with CodeIgniter</h1>
        <?php echo validation_errors(); ?>
        <?php echo form_open('verifylogin'); ?>
        <label for="username">Username:</label>
        <input type="text" size="20" id="username" name="username"/>
        <br/>
        <label for="password">Password:</label>
        <input type="password" size="20" id="passowrd" name="password"/>
        <br/>
        <input type="submit" value="Login"/>
        </form>-->

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
                            <?php echo validation_errors(); ?>
                            <?php echo form_open('verifylogin'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="username" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">
                                        Remember Me </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" value="Login" class="btn btn-lg btn-success btn-block"/>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>