

<html>
    <head>
        <?php echo $library_src?>
        <script>var base_url =  '<?php echo base_url(); ?>';
        </script>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome/css/font-awesome.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/sb-admin.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/dataTables.bootstrap.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css'); ?>"/>

        <script src="<?php echo base_url('assets/js/bootstrap.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/college.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/jquery.dataTables.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/dataTables.bootstrap.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/sb-admin.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/jquery.metisMenu.js'); ?>" type="text/javascript"></script>
        <title>College Details</title>
    </head>
    <body>

        <div id="wrapper">
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">SB Admin v2.0</a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i> </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button> </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('college'); ?>"><i class="fa fa-table fa-fw"></i>College</a>
                        </li>
                    </ul><!--nav-->
                </div><!--sidebar-collapse-->
            </nav><!-- navbar-static-side -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Add/Edit College</h1>
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
                <div class="alert alert-success alert-dismissable" style="display: none">
                	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    Success! Changes save successfully
                </div>
                <div class="alert alert-danger alert-dismissable" style="display:none">
                	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    	Error! Please fill following details.
                   	</div>
                 
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                College Form
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <form name="collegeForm" id="collegeForm">
                                            <input type="hidden" name="collegeId" value="<?php echo $collegeId;?>" />
                                            <div class="form-group validationForName">
                                                <label class="control-label" for="inputError">Name</label>
                                                <input class="form-control" id="collegeName" name="name" placeholder="Enter College Name" value="">
                                            </div>
                                            <div class="form-group validationForAddress">
                                                <label class="control-label" for="inputError" >Address</label>
                                                <textarea class="form-control" rows="3" id="address" name="address"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>City</label>
                                                <select class="form-control" name="city">
                                                    <option>Select City</option>
                                                    <option>Ahmednagar</option>
                                                    <option>Pune</option>
                                                    <option>Mumbai</option>
                                                    <option>Nashik</option>
                                                    <option>Beed</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>State</label>
                                                <select class="form-control" name="state">
                                                    <option>Select State</option>
                                                    <option>Maharashtra</option>
                                                    <option>Gujrat</option>
                                                </select>
                                            </div>
                                            <div class="form-group validationForPhone">
                                                <label>Phone Number</label>
                                                <input class="form-control" id="phone" placeholder="Enter Phone Number" name="phone">
                                            </div>
                                            <div class="form-group validationForCode">
                                                <label>Pin Code</label>
                                                <input class="form-control" id="pinCode" placeholder="Enter Pin Number" name="pinCode">
                                            </div>
                                            <button type="button" class="btn btn-primary" onclick="College.saveCollege();">
                                                Submit
                                            </button>
                                        </form>
                                    </div><!--col-lg-12-->
                                </div><!--row-->
                            </div><!--panel_body-->

                        </div><!--panel-default-->
                    </div><!--col-lg-12-->
                </div><!--row-->

            </div><!--page-wrapper-->
        </div><!--wrapper-->

        <!--<?php echo $this->table->generate();?>-->
    </body>
</html>