<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class College extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('collegeDetails');
		$this->load->library('Datatables');
		$this->load->library('table');
	}
	public function index()
	{
		//$data['library_src'] = $this->jquery->script();
    	$data['script_head'] = $this->jquery->_compile();	
		$this->load->view('college_view',$data);
	}

	public function datatable()
	{
		 $this->collegeDetails->getDatatableRecord();
		
	}
	
	public function addCollege()
	{
		$data['script_head'] = $this->jquery->_compile();	
		$data['collegeId'] = '0';
		$this->load->view('edit_college',$data);
	}
	public function editCollege()
	{
		if ($this->uri->segment(3) != "")
		{
			$collegeId = $this->uri->segment(3);
			$data['script_head'] = $this->jquery->_compile();	
			$data['collegeId'] = $collegeId;	
			$data['collegeDetails'] = $this->collegeDetails->getCollegeDeatilsWithId($collegeId);
			$this->load->view('edit_college',$data);
		}
		
	}
	public function saveCollegeDeails()
	{
		if ($this->input->post('collegeId') == 0)
		{
			$this->collegeDetails->addNewCollege($this->input->post());
			echo $this->input->post('collegeId');
		}
		else if ($this->input->post('collegeId')!=0)
		{
			$this->collegeDetails->updateCollege($this->input->post());
			echo $this->input->post('collegeId');
		}
	}
	public function demo_method()
	{
		for ($i=0;$i<=100007012312346;$i++)
		{
			
		}
		echo "I am here";
	}
}