<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
		
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data['script_head'] = $this->jquery->_compile();	
		$this->load->view('login_view',$data);	
	}
}